package com.solidopinion.tasks.campaigns;

/**
 * Created by Vitaly Romashkin on 27.06.2017.
 */
public class Segment {

    private int number;

    public Segment(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Segment segment = (Segment) o;
        return number == segment.number;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(number);
    }
}

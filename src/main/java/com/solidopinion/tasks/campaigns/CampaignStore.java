package com.solidopinion.tasks.campaigns;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Vitaly Romashkin on 27.06.2017.
 */
public class CampaignStore {

    private Map<Segment, List<Campaign>> store = new HashMap<>();

    public static CampaignStore loadFromFile(File file) throws IOException {

        final CampaignStore store = new CampaignStore();

        try (Scanner scanner = new Scanner(file)) {
            String line;
            String[] values;
            String campaignName;

            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                values = line.split("\\s");
                campaignName = values[0];
                List<Segment> segments = Stream.of(values).skip(1).map(number -> new Segment(Integer.parseInt(number))).collect(Collectors.toList());

                store.addTargetingSegments(new Campaign(campaignName), segments);

            }
        }

        return store;
    }


    public void addTargetingSegment(Campaign campaign, Segment segment) {
        List<Campaign> campaigns = store.get(segment);
        if (campaigns == null) {
            campaigns = new ArrayList<>();
            campaigns.add(campaign);
            store.put(segment, campaigns);
        } else {
            campaigns.add(campaign);
        }
    }

    public void addTargetingSegments(Campaign campaign, List<Segment> segments) {
        for (Segment segment : segments) {
            addTargetingSegment(campaign, segment);
        }
    }


    public Optional<Campaign> getMostAppropriateCampaign(int... segmentNumbers) {

        final Map<Campaign, Integer> searchMap = new HashMap<>();

        List<Segment> segments = IntStream.of(segmentNumbers).mapToObj(value -> new Segment(value)).collect(Collectors.toList());

        segments.forEach(segment -> {
            List<Campaign> campaigns = store.get(segment);
            if (campaigns == null) return;

            campaigns.stream().forEach(campaign -> {
                Integer value = searchMap.get(campaign);
                if (value == null) {
                    searchMap.put(campaign, 1);
                } else {
                    searchMap.put(campaign, value + 1);
                }
            });
        });

        if (searchMap.isEmpty()) {
            return Optional.empty();
        }

        return searchMap
                .entrySet()
                .parallelStream()
                .max(Comparator.comparingInt(Map.Entry::getValue))
                .map(entry -> entry.getKey());

    }


}

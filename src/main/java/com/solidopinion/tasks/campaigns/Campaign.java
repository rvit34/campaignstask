package com.solidopinion.tasks.campaigns;

/**
 * Created by Vitaly Romashkin on 27.06.2017.
 */
public class Campaign {

    private String name;

    private int starvationLevel;

    public Campaign(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStarvationLevel() {
        return starvationLevel;
    }

    public void decrementStarvationLevel() {
        this.starvationLevel -= 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Campaign campaign = (Campaign) o;

        return name != null ? name.equals(campaign.name) : campaign.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}

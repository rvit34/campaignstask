package com.solidopinion.tasks.campaigns;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * Created by Vitaly Romashkin on 27.06.2017.
 */
public class Launcher {

    public static void main(String[] args) {

        File file = getFileFromArgs(args);
        CampaignStore store = loadCampaigns(file);
        processInput(store);
    }


    private static CampaignStore loadCampaigns(File file) {
        CampaignStore store = null;
        try {
            store = CampaignStore.loadFromFile(file);
            System.out.println("campaigns were loaded");
        } catch (IOException e) {
            System.err.println("could not load campaigns from input file");
            System.exit(1);
        }
        return store;
    }

    private static File getFileFromArgs(String[] args) {

        // first just do a bit of some getFileFromArgs
        if (args.length == 0 || args[0] == null || args[0].isEmpty()) {
            System.err.println("you have not specified the path to file with input data");
            System.exit(1);
        }

        String path = args[0];
        File file = new File(path);

        if (!file.exists() || !file.canRead()) {
            System.err.println(String.format("file %s does not exist or cannot be read", path));
            System.exit(1);
        }
        return file;
    }

    private static void processInput(CampaignStore store) {

        System.out.println("input some segments to find appropriate campaign or type quit for exit:");
        try (Scanner scanner = new Scanner(System.in)) {

            String inputLine;
            String[] segments;
            while (scanner.hasNextLine()) {
                inputLine = scanner.nextLine();
                if (inputLine.isEmpty()) {
                    System.out.println("type some search segments derived by space");
                }
                if (inputLine.equalsIgnoreCase("quit")) {
                    System.exit(0);
                }
                segments = inputLine.split("\\s");
                try {
                    int[] segmentNumbers = Stream.of(segments).mapToInt(value -> Integer.parseInt(value)).toArray();

                    Optional<Campaign> campaign = store.getMostAppropriateCampaign(segmentNumbers);
                    if (campaign.isPresent()){
                        System.out.println(campaign.get().getName());
                    }else{
                        System.out.println("no campaign");
                    }

                } catch (Exception e) {
                    System.err.println("incorrect input");
                }
            }
        }
    }


}


package com.solidopinion.tasks.campaigns;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;

/**
 * Created by Vitaly Romashkin on 27.06.2017.
 */
@RunWith(JUnit4.class)
public class CampaignStoreTest {

    private CampaignStore campaignStore;

    @Before
    public void setUp() throws Exception {
        campaignStore = CampaignStore.loadFromFile(new File("src/test/resources/example_data.txt"));
    }

    @Test
    public void testSearch(){

        Optional<Campaign> campaign = campaignStore.getMostAppropriateCampaign(3, 4, 5, 10, 2, 200);
        Assert.assertTrue(campaign.isPresent());
        Assert.assertThat(campaign.get().getName(),is("campaign_a"));

        campaign = campaignStore.getMostAppropriateCampaign(3);
        Assert.assertTrue(campaign.isPresent());
        Assert.assertThat(campaign.get().getName(),anyOf(is("campaign_a"),is("campaign_b"),is("campaign_c")));

        campaign = campaignStore.getMostAppropriateCampaign(4, 10, 15);
        Assert.assertTrue(campaign.isPresent());
        Assert.assertThat(campaign.get().getName(),is("campaign_a"));

        campaign = campaignStore.getMostAppropriateCampaign(1024, 15, 200, 21, 9, 14, 15);
        Assert.assertTrue(campaign.isPresent());
        Assert.assertThat(campaign.get().getName(),is("campaign_b"));

        campaign = campaignStore.getMostAppropriateCampaign(9000, 29833, 65000 );
        Assert.assertFalse(campaign.isPresent());

    }
}